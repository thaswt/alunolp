object DataModule4: TDataModule4
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 215
  Width = 241
  object FDConnection1: TFDConnection
    Params.Strings = (
      'User_Name=root'
      'Database=PokeBase'
      'DriverID=MySQL')
    Connected = True
    Left = 152
    Top = 32
  end
  object DataSource1: TDataSource
    DataSet = FDQuery1
    Left = 56
    Top = 128
  end
  object FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink
    VendorLib = 'C:\EasyPHP-DevServer-14.1VC9\binaries\mysql\lib\libmysql.dll'
    Left = 56
    Top = 32
  end
  object FDQuery1: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select * from treinador')
    Left = 56
    Top = 80
  end
  object FDQuery2: TFDQuery
    Active = True
    Connection = FDConnection1
    SQL.Strings = (
      'select * from pokemon')
    Left = 152
    Top = 80
  end
  object DataSource2: TDataSource
    DataSet = FDQuery2
    Left = 152
    Top = 128
  end
end
